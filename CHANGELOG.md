# CHANGELOG

<!--- next entry here -->

## 2.0.0
2020-05-27

### Breaking changes

#### **project:** Follow Client (ac3425347a45a0d993e90973050f12fe35455ef4)

Follow changes on electron client

- Deploy Client's changes on web interface

### Features

- **electron:** added commands and installers electron. (37e6fb4d05cccfbf8fcef32c038e3ec34f2b0b47)

### Fixes

- **ci:** Fixing electron-builder error (98ee993e5ad4536b08dcd3a79cf270494a45b08a)
- **ci:** Add dev deps (fac20f818a0c9b4107bfcdde49658b51c73270d9)

## 1.2.0
2020-05-12

### Features

- **electron:** Added electron to actual angular project. (3edd20a5f3665d659dea247de2cd79fc708f8f09)

## 1.1.0
2020-05-12

### Features

- **electron:** Added electron to actual angular project. (b7ed15190310d176a12a81276b81c02ac3b33baa)

## 1.0.1
2020-05-11

### Fixes

- **deployment:** Change image url (f838061a6810dc95a6b8a444c3515122ef683672)

## 1.0.0
2020-05-11

### Fixes

- **public:** Change public url in kubernetes ingress (830464c66a9d6ab4a04dc1ec58ad506333b4a38d)

## 0.8.0
2020-04-13

### Features

- **gitaha:** All commits canceled so.. (715554f890a04146308c161f2763fb31af87e01d)

## 0.7.2
2020-04-13

### Fixes

- **environment:** Changed Api urls for prodution. (082f4966b9a3e7ab041173929f92d8626bb0be3f)

## 0.7.1
2020-04-13

### Fixes

- **prod:** Change deployment method (0aa668dc44ee46e8c92aff82bdaf4484b3f92c59)

## 0.7.0
2020-04-13

### Features

- **logincomponent:** Add Forgot password link + manual user login right after login state. (1c07d4068c98e75ce17cf2cffc170dd572bac297)
- **posteservice:** Adding all functions to manage postes. (5847a6c7591fda2c25006a85efdb52625e1b9183)
- **adminpersonneladdcomponent:** Added success and error request handling. (cfdaab225789e8ec6dfb351762f1f9670ea4d0f9)
- **adminpersonnelviewcomponent:** Added search for personnels by username. (a2b62f93ff6f411dc62e61c67557d3708c7f2710)
- **adminposteaddcomponent:** Added success and error request handling. (a8c95fd7c1930094aa2167ae06a6641aa4ae6067)
- **adminposteviewcomponent:** Added search by poste name. (520e70115969e962c4c1cfc775b9a451bb8d873a)
- **adminequipeviewcomponent:** Fully finished view equipe component (search + display). (bffc9d000602271bfeafc7dd20e6bac2bdfeb678)
- **adminequipeaddcomponent:** fully finised equipe add component. (c4df0cf90f53d432dc8347e63ae693566c7c73cf)
- **adminequipeeditcomponent:** fully finished equipe edit component. (27149603b87a4ca0ca63e13e2aecc837479c4893)
- **createdirective:** Added a element Create event to the virtual dom. (90c46b6e64d1abeb335e665fe60c41696df8e8ac)

### Fixes

- **headerusersectioncomponent:** Redirect to refresh routes after logout. (56ae255923b4454bc697a04f550e015ec20897ca)
- **authservice:** AuthState not working every times. (939c3f70515c9bd8a7afe9766afa4c9e2f4cbbf7)
- **personnelservice:** Change search by username strict function. (a197ff5182a18ee19892f656012f0939c8df670d)
- **loginguard:** Simplify login guard. (956cc5eed9a9c402c67878c24b70ffca7c427cbd)
- **adminguard:** Minor changes. (01dd762f5151e8bc45fc45533c4a430016fb72ce)
- **userroutes:** Unique route for equipe edit. (91bf786580d2be1c75745d72109cb62bf8caa35c)

## 0.6.0
2020-04-10

### Features

- **deployment:** Update image on ci (51e428354b01fdcad07a0da31f979d941adc4e67)

## 0.5.0
2020-04-09

### Features

- **equipeaddcomponent:** Front fully ready. (21d8f6479b94920030c02d5ed380d76b82bb50bc)
- **posteservice:** Add EquipeAdd function. (16d361827ff4a8d5beb1d0b19e7b5e4b8fb54514)

### Fixes

- **roleadminmenu:** Delete roles from Admin menu. (b3c78706d676ff2cb34ba7377b89d3df96285fdd)
- **components:** Delete old console logs. (fe33343f63561f2c3921b7f99618640e22fe35f8)
- **npmupdate:** update of all packages (a0321b742fa24e54886e2067a1fc956ec30598cb)

## 0.4.1
2020-04-09

### Fixes

- **npm:** update project packages (1ad7efebcb4260315da8964d31d1c4600584914e)

## 0.4.0
2020-04-09

### Features

- **adminmenucomponent:** Adding routeLinks to Admin menu + removing Centre section. (c69af8b7e5eb969f857a034ee86fe65c23df10f1)
- **customvalidator:** Add containSpace validator for formControl. (30b801c29a79fe04e7be5cc2e1029303f6197268)
- **personnelservice:** Add some functions plus refactor the code. (718f043bd2f9ad1fa2af717340dbb864a8d3ec22)
- **personnelservice:** Add some functions plus refactor the code. (88ba8b550bbd73618748d1414db666b1c34870bd)
- **equipeaddcomponent:** Add Equipe Add Component. (3beffdd8d10aac24d0e15a2d614b2a38fdf83244)
- **equipeeditcomponent:** Add Equipe Edit Component. (1930e0419c8c14bf879be3f0c3083afaa748e785)
- **equipeviewcomponent:** Add Equipe View Component. (e5c43ad913e84c1e4130e03a2e0827215c80ffbb)
- **posteaddcomponent:** Add Poste Add Component. (5bb681d4e2748c0931e748e7034dee14a13e0880)
- **posteeditcomponent:** Add Poste Edit Component. (d3a4d6bd4f0a356c66b0ac3f4e913ddd55e2505c)
- **posteviewcomponent:** Add Poste View Component. (afec87a34a1060980fe9fc4285ddd33e26fb9e49)
- **posteservice:** Add Poste Service. (94b8dfe1ad917ea76aee045686b08b2f51a3f341)
- **posteeditcomponent:** add buttons to edit responsable in poste. (84a461b8c46fbc13b5ea5366e553c38e684b05fd)
- **posteservice:** Add function to get the responsable of a given poste.service.ts. (c3a36e1e354578ce2f3ea103bb40770ccaa18790)

### Fixes

- **personneladdcomponent:** Preventing spaces in username + some changes in form encoding to request body. (11184c655402afbd5bc884d8a72b2e6c7d94c1f1)
- **personneleditcomponent:** Changing input submit to button tag. (85ce88fb6f0ae6f6eca867e347f601036dc76263)
- **personnelviewcomponent:** fix loading animation on the table + delete logs in console. (64d27e88dfd8fcb1442aac35227179974a472ba0)
- **posteaddcomponent:** Fix input search size. (8bdd358368799c3df12011414fce9f0ac40c938d)
- **personnelservice:** Bad pull request. (27fd66df4fd22cc4fdb9484e03e6ea0aeeb2b75b)
- **environment:** Api Urls in production environment. (d26fea21ccf8e23ac05baf7ff0ed544e73b79dc3)

## 0.3.1
2020-04-08

### Fixes

- **deployment:** repair deployment (d65ff633fc53f3e6b75baba4292495bc40b60853)
- **licence:** Add LICENCE (f60c508fec10df3005e61218f7ea6d4c0894a848)

## 0.3.0
2020-04-07

### Features

- **tls:** Add certificate (6829382320b274c0e0a1423dadb42348128a9c57)

## 0.2.3
2020-04-07

### Fixes

- **ci:** rerun ci (6333949a2e3e2ec4f80994fb39818bc15f227f05)

## 0.2.2
2020-04-07

### Fixes

- **ssl:** Try get certificate (f6d98ceddfe57de501e6959f9bcd0458e6985c54)
- **ssl:** Try get certificate (e413100d0a61ac02cf7df162dd80cd4b1f3951f0)

## 0.2.1
2020-04-07

### Fixes

- **ssl:** Change certificate issuer (cf3048a971334e48124f6fa5e09ac8f80f92fa26)

## 0.2.0
2020-04-07

### Features

- **prod:** Add Ingress for external access (92aa73f179a25ecfbf81b2bdec23231533f713d0)

## 0.1.3
2020-04-07

### Fixes

- **container:** Repair container (4ef0a0c597e638854bddca304e8f7aa92f5c6c2f)

## 0.1.2
2020-04-07

### Fixes

- **container:** Change dir in Dockerfile (ce42e0aae08fa7bf151d72c02005e7645886e414)

## 0.1.1
2020-04-06

### Fixes

- **ci:** Remove download in changelog (a34b5edf8d0fd983ba71f7211215ded83dc9ff4d)

## 0.1.0
2020-04-06

### Features

- **app:** creation of angular stater project (b2f44216daa4c468c6a27bc7da7d2781d536a6cf)
- **global:** add some dependencies, and importing Angular FormControl. (2ee2319ccb13e97b9df4f06c284c736509d68342)
- **auth:** Add firebase SDK. (e861624076ab9ccff7ecb4979fca81ba711f1831)
- **auth:** Firebase deployment files. (1346cc0dc5071c2c884deed1b31a594842101603)
- **footer:** Adding simple footer component. (de780cb6d6cc85c62a69f88c4e4c5627006df02b)
- **header:** Adding header. (89491d81421f15cc8d658cad05c15c0d65d3c776)
- **routes:** Adding login form route. (a7c6324b42c04a36e5c620c77ea6d830ff8a34db)
- **auth:** Add fully finished reactive login form, connected to firebase. (fd6e57438d514f85134597cb1a92b8a07fdff2cf)
- **header:** added dropdown account, and different user section if logged in (4425b000dee4f41e8e7703dd76686fcb5745e973)
- **auth:** Redo Firebase Auth as a Service + added it to login page (3f14237bc42e9304d1e5ebe13c0d4561f8f9edde)
- **headerusersection:** Created independant user section component. (ad00c824f1daaa8fcd69f91889b163aecf2dd0ad)
- **appmodule:** added components, and routes. (b59938a400ed8c0cfea3d79a180f1b8d3dc3d264)
- **userdashboard:** Added User Dashboard. (981f4886cf6208b310683c56326dba3e0d88b84c)
- **admindashboard:** Added Admin Dashboard. (6259907f0916e6fdd52d4e5c168ec492449445e3)
- **admindashboard:** Added Left Menu in Admin Dashboard. (0b9628adf3b6dab3892007175c496d8b1bd791f6)
- **adminguard:** Added Admin Route Guard. (6ca9bf0b5a482db132378d259ac52fd6afbd3054)
- **adminguard:** Added Auth Route Guard. (4cb80b28ac65228fcd0badc8568165bae4628f6d)
- **userroutes:** Added User Routes. (4b7531a4d018ffb3a8e208bc6c56cf5a5c31da50)
- **dependencies:** Add angular cli (dc7ae2ef42b453138d45ea571b907685e1335df3)
- **script:** Add deploy command (e587a7f982d10b0458143bf190cc171b50431b5f)
- **update:** update project files (f893a9f2c2d591bb00562d090126fe35dd689ff6)
- **guardsroutes:** Direct landing on login. (b0b21dfd905dc172903948eafadf78ea949547e0)
- Add to idea master project (7105d02e6aed62a7449ca4a9539a84bef3174dd5)
- **ci:** Add ci (d0e21004c35a37ac4af3a8c3fd907b9dfd6d0ba2)
- **deployment:** Add deployment in gitlab ci (7ff88cac7a626b72c30f43bb5d5a59b74cc46895)
- **routes:** Routes for edit and view. (77f9f70d56b5dc3cccea9629248e8ce9d782ff80)
- **validator:** Custom validator. (86377fce79abf008e126ee55c45c8310820a8e48)
- **validator:** Custom validator. (73972e013910539939f131537d5aff76540a47a7)
- **addpersonnelcomponent:** Added Compoenent to add personnel in admin dashboard. (30be2d05fe66163d729da3d6e0e6ef5c84af569e)
- **editpersonnelcomponent:** Added Compoenent to edit personnel in admin dashboard. (06d82c1deb94e8d24152aff10aa5032aaad50b28)
- **viewpersonnelcomponent:** Added Compoenent to view all personnel in admin dashboard. (1e51d0890f10db625e3a1f077894984b3313ecf7)
- **customvalidator:** Update custom validator. (13bd2026200e6b2c0c2da7c510bf15c1af9bc56c)
- **personnelmodel:** Create perosnnel model. (b8ecf4a06067bb1b878f20fc4468f38711773567)
- **personnelservice:** Create personnel Service. (259287eb085ecd5c57803d0e6050f6a36c48adbf)
- **environment:** Update Prod environment variables. (fbb20c9c82fc9a8d0c1db3588296e8a52ea403f2)
- **npm:** Add build tools (93a4e6dcffe5f60cf1ae0ca051c2bae1579ccd4d)
- **ci:** Remove test (9828aa1edefddaedf57d0ace8528c511d89599b6)

### Fixes

- **logincomponent:** fixed getters to return object of FormGroup, not their value (d548927ef8fa0583044981fad2ca08ceb6227095)
- **logincomponent:** fixed account dropdown now working (a1c4cfb04580be61c6411b7699125ff61f89483c)
- **authservice:** Moved AuthService, Sorting files (bf0a3380a53f2459e5b0673945510c078dde8216)
- **headercomponent:** Moved user section to new Component. (e1eace37aa037a835158ec3a750250f22bd6f5a7)
- **login:** Changed files path after refactor (586c3ff6bd04cccd91e7fd2b9d14b04311814275)
- **authroutes:** removing unused import (76cfd014753ae033ebf22438040ff9acaa8f9b2b)
- **routes:** Moved Routes to routing module of angular, instead of app module. (0a630f93a8c03e892f4d48e64ffbaa2b37dcb515)
- **global:** refactor on routes and minor css changes. (a759a4499bfa6e59c3d5b5e2e81f64b1c16bd4c1)
- **branch:** Old Files (must be imported) (3e7276b7e48241de16e4f9d0543123e58e69b0cb)
- **ci:** Update ci (e41729481a1748b7b2f69d34d3fd11d096de857f)
- **adminmenucomponent:** Removed edit and delete pages. (a667a54bcc3b0135bc7b29e3fd043fa4f7183fc4)
- **ci:** keep nodes_modules (85bb686bd8e37e198ed78474a24268d6c4d00ed9)
- **ci:** Repair CI (69c203955e0d215a455db9808451715db03aa68a)
- **ci:** Repair CI (de3546f5ecb29b53ed47c65bfc4344d1b9456d16)