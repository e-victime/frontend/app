// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    firebaseConfig: {
        apiKey: 'AIzaSyBAO0DZbQx6dphpkQsu8ZbJK9WfIqDNrPk',
        authDomain: 'e-victime.firebaseapp.com',
        databaseURL: 'https://e-victime.firebaseio.com',
        projectId: 'e-victime',
        storageBucket: 'e-victime.appspot.com',
        messagingSenderId: '77239354630',
        appId: '1:77239354630:web:f7d955d48a453d19d3ca87',
        measurementId: 'G-4G8DFNBN2N'
    },
    auth: {
        redirectAfterLogin: '/dashboard',
    },
    Api: {
        Urls: {
            personnel: 'https://api.e-victime.antoinethys.com/personnel/',
            poste: 'https://api.e-victime.antoinethys.com/poste/',
            victime: 'https://api.e-victime.antoinethys.com/victime/',
        }
    }

};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
