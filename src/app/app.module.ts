// Angular components

import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

// Environment variables

import {environment} from "../environments/environment";

// Created components

import {HeaderComponent} from './components/header/header.component';
import {HeaderUserSectionComponent} from "./components/header-user-section/header-user-section.component";
import {LoginComponent} from './components/login/login.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {AdminComponent} from './components/admin/admin.component';
import {AdminMenuComponent} from './components/admin-menu/admin-menu.component';
import {AdminPersonnelAddComponent} from './components/admin-personnel-add/admin-personnel-add.component';
import {AdminPersonnelViewComponent} from './components/admin-personnel-view/admin-personnel-view.component';

// Firebase

import {AngularFireModule} from "@angular/fire";
import {AngularFireAuthModule} from "@angular/fire/auth";
import {AuthService} from "./services/auth/auth.service";
import {HttpClientModule} from "@angular/common/http";
import {PersonnelService} from "./services/personnel/personnel.service";
import {CustomValidator} from "./helpers/custom-validator";
import {AdminPersonnelEditComponent} from './components/admin-personnel-edit/admin-personnel-edit.component';
import {AdminEquipeAddComponent} from './components/admin-equipe-add/admin-equipe-add.component';
import {AdminEquipeEditComponent} from './components/admin-equipe-edit/admin-equipe-edit.component';
import {AdminEquipeViewComponent} from './components/admin-equipe-view/admin-equipe-view.component';
import {AdminPosteAddComponent} from './components/admin-poste-add/admin-poste-add.component';
import {AdminPosteEditComponent} from './components/admin-poste-edit/admin-poste-edit.component';
import {AdminPosteViewComponent} from './components/admin-poste-view/admin-poste-view.component';
import {PosteService} from "./services/poste/poste.service";
import {CreateDirective} from "./directives/create/create.directive";
import {DashboardPosteEquipeViewComponent} from './components/dashboard-poste-equipe-view/dashboard-poste-equipe-view.component';
import {DashboardCurrentPosteComponent} from './components/dashboard-current-poste/dashboard-current-poste.component';
import {VictimeDeclarationComponent} from './components/victime-declaration/victime-declaration.component';
import {VictimeComponent} from './components/victime/victime.component';
import {VictimeService} from "./services/victime/victime.service";

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HeaderUserSectionComponent,
        LoginComponent,
        DashboardComponent,
        AdminComponent,
        AdminMenuComponent,
        AdminPersonnelAddComponent,
        AdminPersonnelViewComponent,
        AdminPersonnelEditComponent,
        AdminEquipeAddComponent,
        AdminEquipeEditComponent,
        AdminEquipeViewComponent,
        AdminPosteAddComponent,
        AdminPosteEditComponent,
        AdminPosteViewComponent,
        CreateDirective,
        DashboardPosteEquipeViewComponent,
        DashboardCurrentPosteComponent,
        VictimeDeclarationComponent,
        VictimeComponent,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        AngularFireAuthModule,
        AngularFireModule.initializeApp(environment.firebaseConfig),
    ],
    providers: [
        AuthService,
        PersonnelService,
        PosteService,
        CustomValidator,
        VictimeService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
