import {Directive, ElementRef, EventEmitter, Output} from '@angular/core';

@Directive({
    selector: '[Create]'
})
export class CreateDirective {

    @Output() Create: EventEmitter<any> = new EventEmitter<any>();

    constructor(private elRef: ElementRef) {
    }

    ngOnInit() {
        this.Create.emit(this.elRef);
    }

}
