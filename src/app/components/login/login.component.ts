import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    AuthFailed: string;
    AuthSuccess: Object;

    constructor(private fb: FormBuilder, private auth: AuthService, private router: Router) {
    }

    ngOnInit(): void {
        this.loginForm = this.fb.group({
            email: [
                '',
                [
                    Validators.email,
                    Validators.required
                ]
            ],
            password: [
                '',
                [
                    Validators.required,
                    Validators.minLength(6)
                ]
            ]
        });

    }

    get email() {
        return this.loginForm.get('email');
    }

    get password() {
        return this.loginForm.get('password');
    }

    submit() {
        document.querySelector('#submit').classList.toggle('loading');

        this.auth.login(this.email.value, this.password.value)
            .then(result => {
                document.querySelector('#submit').classList.toggle('loading');

                delete this.AuthFailed;
                this.AuthSuccess = result;

                this.auth.refreshAuthState().subscribe(value => {

                    this.auth.manualDefineUser(value);

                    this.router.navigateByUrl(this.auth.redirect());
                });

            })
            .catch(error => {
                document.querySelector('#submit').classList.toggle('loading');
                if (error.code === 'auth/internal-error') {
                    this.AuthFailed = "Une erreur est intervenue sur le serveur d'authentification";
                } else if (error.code === 'auth/invalid-email' || error.code === 'auth/user-not-found') {
                    this.AuthFailed = "Vôtre combinaison Email et mot de passe est fausse";
                } else if (error.code === 'auth/invalid-email-verified') {
                    this.AuthFailed = "Vous n'avez pas vérifié votre Email !"
                } else {
                    this.AuthFailed = error.message;
                }
            });

    }

}
