import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPosteViewComponent } from './admin-poste-view.component';

describe('AdminPosteViewComponent', () => {
  let component: AdminPosteViewComponent;
  let fixture: ComponentFixture<AdminPosteViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPosteViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPosteViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
