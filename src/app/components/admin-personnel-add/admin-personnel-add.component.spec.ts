import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPersonnelAddComponent } from './admin-personnel-add.component';

describe('AdminPersonnelAddComponent', () => {
  let component: AdminPersonnelAddComponent;
  let fixture: ComponentFixture<AdminPersonnelAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPersonnelAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPersonnelAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
