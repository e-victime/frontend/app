import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidator} from '../../helpers/custom-validator';
import {PersonnelService} from '../../services/personnel/personnel.service';
import {HttpClient} from '@angular/common/http';

declare let $: any;

@Component({
    selector: 'app-admin-personnel-add',
    templateUrl: './admin-personnel-add.component.html',
    styleUrls: ['./admin-personnel-add.component.scss']
})

export class AdminPersonnelAddComponent implements OnInit {

    personnelForm: FormGroup;
    success: object;
    error: object;


    constructor(private fb: FormBuilder, private personnelService: PersonnelService, private http: HttpClient, private CV: CustomValidator) {
    }

    ngOnInit(): void {

        // JQUERY SEMANTIC CSS

        this.jqueryInit();

        // FORM

        this.personnelForm = this.fb.group(
            {
                firstname: ['',
                    [
                        Validators.required
                    ]
                ],
                lastname: ['',
                    [
                        Validators.required
                    ]
                ],
                phone: ['',
                    [
                        Validators.required,
                        Validators.minLength(9),
                        Validators.maxLength(9)
                    ]
                ],
                gender: ['',
                    [
                        Validators.required,
                    ]
                ],
                birthdate: ['',
                    [
                        Validators.required,
                    ]
                ],
                username: ['',
                    [
                        this.CV.containSpace,
                        Validators.required,
                        Validators.minLength(5),
                        Validators.maxLength(30),
                        this.CV.usernameUsedValidator
                    ]
                ],
                email: ['',
                    [
                        Validators.required,
                        Validators.email
                    ]
                ],
                password: ['',
                    [
                        Validators.required,
                        Validators.minLength(6)
                    ]
                ],
                passwordConfirm: ['',
                    [
                        Validators.required
                    ]
                ],
            },
            {
                validators: this.CV.passwordConfirm
            }
        );

    }

    hasProp(o, name) {
        return o.hasOwnProperty(name);
    }

    get form() {
        return this.personnelForm.controls;
    }

    formEncoded() {
        const username: string = this.form.username.value;
        let firstname: string = this.form.firstname.value;
        firstname = firstname.replace(firstname.charAt(0), firstname.charAt(0).toUpperCase());
        let lastname: string = this.form.lastname.value;
        lastname = lastname.replace(lastname.charAt(0), lastname.charAt(0).toUpperCase());
        return {
            username: username.toLowerCase(),
            civilite: this.form.gender.value,
            prenom: firstname,
            nom: lastname,
            dateNaissance: this.form.birthdate.value,
            actif: true,
            email: this.form.email.value,
            password: this.form.password.value,
            phone: this.form.phone.value
        };
    }

    onSubmit() {

        this.error = null;
        this.success = null;

        document.getElementById('submit-form').classList.toggle('loading');

        this.personnelService.add(this.formEncoded())
            .subscribe(r => {
                    this.success = r;
                    document.getElementById('submit-form').classList.toggle('loading');
                    this.form.password.setValue('');
                    this.form.passwordConfirm.setValue('');
                    this.personnelForm.reset();
                },
                error => {
                    this.error = error;
                    console.log(error);
                    document.getElementById('submit-form').classList.toggle('loading');
                    this.form.password.setValue('');
                    this.form.passwordConfirm.setValue('');
                    this.personnelForm.reset();
                },
            );

    }

    jqueryInit(): void {
        $('.ui.dropdown').dropdown();
    }

}
