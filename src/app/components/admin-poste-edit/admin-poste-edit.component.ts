import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidator} from '../../helpers/custom-validator';
import {ActivatedRoute} from '@angular/router';
import {PosteService} from '../../services/poste/poste.service';
import {environment} from '../../../environments/environment';

declare let $: any;

@Component({
    selector: 'app-admin-poste-edit',
    templateUrl: './admin-poste-edit.component.html',
    styleUrls: ['./admin-poste-edit.component.scss']
})
export class AdminPosteEditComponent implements OnInit {

    posteForm: FormGroup;
    response: object;

    posteId: string = this.router.snapshot.paramMap.get('id');

    constructor(private fb: FormBuilder, private CV: CustomValidator, private router: ActivatedRoute, private posteService: PosteService) {
    }

    ngOnInit(): void {

        // JQUER INIT

        this.jqueryInit();

        // FORM

        this.posteForm = this.fb.group(
            {
                posteName: ['',
                    [
                        Validators.required
                    ]
                ],
                responsable: ['',
                    []
                ],
                dateBegin: ['',
                    [
                        Validators.required
                    ]
                ],
                dateEnd: ['',
                    [
                        Validators.required
                    ]
                ],
                location: ['',
                    [
                        Validators.required
                    ]
                ],
                description: ['',
                    [
                        Validators.required
                    ]
                ]
            }
        );

        // FILL FORM WITH POSTE VALUES

        this.posteService.get(this.posteId)
            .subscribe(r => {

                this.form.posteName.setValue(r.nom);
                this.form.dateBegin.setValue(r.dateDebut.replace(' ', 'T').slice(0, r.dateDebut.length - 3));
                this.form.dateEnd.setValue(r.dateFin.replace(' ', 'T').slice(0, r.dateFin.length - 3));
                this.form.location.setValue(r.location);
                this.form.description.setValue(r.descriptionPoste);

            });

        this.posteService.getResponsable(this.posteId)
            .subscribe(r => {

                $('.ui.dropdown .default.text')
                    .text(`${r.nom.toUpperCase()} ${r.prenom} "${r.username}"`);

                // A finir

            });

    }

    editResponsable() {

        $('.ui.search.selection.dropdown').toggleClass('disabled');
        $('.ui.dropdown .default.text').text('Nom d\'utilisateur du responsable');
        $('.poste-responsable .ui.large.green.button').toggleClass('hidden');
        $('.poste-responsable .ui.large.red.button').toggleClass('hidden');
        $('.poste-responsable .ui.large.blue.button').toggleClass('hidden');

    }

    deleteResponsable() {

        $('.ui.search.selection.dropdown')
            .dropdown('clear');

    }

    rollbackResponsable() {


        $('.ui.search.selection.dropdown')
            .toggleClass('disabled')
            .dropdown('clear');

        this.posteService.getResponsable(this.posteId)
            .subscribe(r => {
                $('.ui.dropdown .default.text')
                    .text(`${r.nom.toUpperCase()} ${r.prenom} "${r.username}"`);
                this.form.responsable.setValue('');

            });

        $('.poste-responsable .ui.large.red.button').toggleClass('hidden');
        $('.poste-responsable .ui.large.blue.button').toggleClass('hidden');
        $('.poste-responsable .ui.large.green.button').toggleClass('hidden');
    }

    hasProp(o, name) {
        return o.hasOwnProperty(name);
    }

    get form() {
        return this.posteForm.controls;
    }

    formEncoded() {
        return {};
    }

    onSubmit() {

        document.getElementById('submit-form').classList.toggle('loading');

        // REQUEST

        document.getElementById('submit-form').classList.toggle('loading');

    }

    jqueryInit(): void {
        $('.ui.dropdown')
            .dropdown({
                onChange: (val) => {
                    this.form.responsable.setValue(val);
                },
                message: {
                    noResults: 'Pas de résultats trouvés...'
                },
                apiSettings: {
                    throttleFirstRequest: false,
                    throttle: 0,
                    minCharacters: 1,
                    url: environment.Api.Urls.personnel + 'search?search=(username:{query}* OR username:*{query}*)',
                    onResponse: (r) => {

                        const response = {
                            results: []
                        };

                        for (const personne of r.content) {

                            const result = {
                                name: '',
                                value: ''
                            };

                            result.name = `${personne.nom.toUpperCase()} ${personne.prenom} "${personne.username}"`;
                            result.value = personne.id;

                            response.results.push(result);

                        }

                        return response;

                    }
                }
            });

    }

}
