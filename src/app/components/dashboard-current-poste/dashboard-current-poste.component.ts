import {Component, OnInit} from '@angular/core';
import {PersonnelService} from "../../services/personnel/personnel.service";
import {PosteService} from "../../services/poste/poste.service";
import {AuthService} from "../../services/auth/auth.service";

@Component({
    selector: 'dashboard-current-poste',
    templateUrl: './dashboard-current-poste.component.html',
    styleUrls: ['./dashboard-current-poste.component.scss']
})
export class DashboardCurrentPosteComponent implements OnInit {

    equipesId: Array<number> = [];
    ownEquipe: object;
    postesId: Array<number> = [];
    poste: object;
    posteNom: string;
    posteDateDebut: string;
    posteDateFin: string;
    posteLocation: string;
    equipes: Array<object> = [];
    user: object;
    victimePosteId: number;
    victimeEquipeId: number;

    constructor(private personnelService: PersonnelService, private posteService: PosteService, private auth: AuthService) {
    }

    async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    ngOnInit(): void {

        this.personnelService.getByEmailStrict(this.auth.currentUser.email)
            .subscribe(
                value => {
                    this.user = value.content[0];

                    // @ts-ignore
                    this.posteService.getEquipesOfPersonnel(`${this.user.id}`)
                        .subscribe(
                            async value1 => {

                                await this.asyncForEach(value1.content, async (element) => {

                                    // @ts-ignore
                                    let equipes = await this.posteService.getEquipeById(element.id.equipeId).toPromise().then(equipe => equipe.content);

                                    for (let equipe of equipes) {
                                        this.equipesId.push(equipe.id);
                                        this.postesId.push(equipe.posteId);
                                    }

                                });

                                await this.asyncForEach(this.postesId, async (element) => {

                                    let poste = await this.posteService.get(element).toPromise().then(value2 => value2);

                                    let dateDebut = new Date(poste.dateDebut).getTime();
                                    let dateFin = new Date(poste.dateFin).getTime();
                                    let dateMaintenant = new Date().getTime();

                                    if (dateDebut < dateMaintenant && dateMaintenant < dateFin) {
                                        this.poste = poste;
                                        this.posteDateDebut = poste.dateDebut;
                                        this.posteDateFin = poste.dateFin;
                                        this.posteLocation = poste.location;
                                        this.posteNom = poste.nom;
                                        this.victimePosteId = poste.id;
                                    }

                                });

                                if (this.poste) {

                                    // @ts-ignore
                                    this.posteService.getEquipesOfPoste(this.poste.id)
                                        .subscribe(
                                            value2 => {
                                                this.equipes = value2.content;

                                                console.log(this.equipesId, value2.content);

                                                this.equipesId.forEach(element => {
                                                    value2.content.forEach(element2 => {
                                                        if (element === element2.id) {
                                                            this.victimeEquipeId = element2.id;
                                                        }
                                                    });
                                                });

                                            }
                                        );

                                }

                            },
                            error => console.log(error)
                        );

                },
                error => console.log(error)
            );

    }

}
