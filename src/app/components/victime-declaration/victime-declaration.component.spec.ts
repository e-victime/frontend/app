import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VictimeDeclarationComponent } from './victime-declaration.component';

describe('VictimeDeclarationComponent', () => {
  let component: VictimeDeclarationComponent;
  let fixture: ComponentFixture<VictimeDeclarationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VictimeDeclarationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VictimeDeclarationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
