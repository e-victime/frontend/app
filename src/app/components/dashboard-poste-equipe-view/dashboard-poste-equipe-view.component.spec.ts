import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DashboardPosteEquipeViewComponent } from './dashboard-poste-equipe-view.component';

describe('DashboardPosteEquipeViewComponent', () => {
  let component: DashboardPosteEquipeViewComponent;
  let fixture: ComponentFixture<DashboardPosteEquipeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashboardPosteEquipeViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPosteEquipeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
