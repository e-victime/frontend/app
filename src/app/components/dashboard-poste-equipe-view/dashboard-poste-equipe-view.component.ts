import {Component, OnInit} from '@angular/core';
import {PersonnelService} from '../../services/personnel/personnel.service';
import {PosteService} from '../../services/poste/poste.service';
import {AuthService} from '../../services/auth/auth.service';

declare let $: any;

@Component({
    selector: 'dashboard-poste-equipe-view',
    templateUrl: './dashboard-poste-equipe-view.component.html',
    styleUrls: ['./dashboard-poste-equipe-view.component.scss']
})
export class DashboardPosteEquipeViewComponent implements OnInit {

    postesId: Array<number> = [];
    postes: Array<object> = [];
    equipes: Array<object> = [];
    user: object;

    constructor(private personnelService: PersonnelService, private posteService: PosteService, private auth: AuthService) {
    }

    async asyncForEach(array, callback) {
        for (let index = 0; index < array.length; index++) {
            await callback(array[index], index, array);
        }
    }

    ngOnInit(): void {

        // INIT VARIABLES

        this.personnelService.getByEmailStrict(this.auth.currentUser.email)
            .subscribe(
                value => {
                    this.user = value.content[0];

                    // @ts-ignore
                    this.posteService.getEquipesOfPersonnel(`${this.user.id}`)
                        .subscribe(
                            async value1 => {

                                await this.asyncForEach(value1.content, async (element) => {

                                    // @ts-ignore
                                    const equipes: Array = await this.posteService.getEquipeById(element.id.equipeId).toPromise().then(equipe => equipe.content);

                                    equipes.forEach((element, index) => {
                                        this.postesId.push(element.posteId);
                                    });

                                });

                                await this.asyncForEach(this.postesId, async (element) => {

                                    const poste = await this.posteService.get(element).toPromise().then(value2 => value2);

                                    this.postes.push(poste);

                                });

                            },
                            error => console.log(error)
                        );

                },
                error => console.log(error)
            );

    }

    displayEquipes(poste) {

        this.posteService.getEquipesOfPoste(poste.id)
            .subscribe(
                value => {
                    this.equipes = value.content;
                    $('.ui.small.header.equipes-header').text('Equipes du poste "' + poste.nom + '"');
                }
            );

    }

}
