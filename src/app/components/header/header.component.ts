import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';

declare let $: any;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    clientWidth: number;
    responsiveOpen: boolean = false;

    constructor(public auth: AuthService) {
    }

    ngOnInit(): void {

        this.clientWidth = window.innerWidth;

        window.addEventListener('resize', () => {
            this.clientWidth = window.innerWidth;

            if (window.innerWidth > 768) {
                if (this.responsiveOpen) {
                    this.toggleResponsiveMenu();
                }
            }

        });

    }

    logout(): void {
        this.auth.logout();
    }

    toggleResponsiveMenu(): void {

        document.body.style.overflow = 'hidden';
        this.responsiveOpen = !this.responsiveOpen;

        if (this.responsiveOpen) {
            $('#responsive-menu').css({
                left: '0'
            });
            $('html, body').css({
                overflow: 'hidden',
                height: '100%'
            });
        } else {
            $('#responsive-menu').css({
                left: '100vw'
            });
            $('html, body').css({
                overflow: 'auto',
                height: 'auto'
            });
        }

    }


}
