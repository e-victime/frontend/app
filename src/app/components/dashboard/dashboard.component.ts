import {Component, OnInit} from '@angular/core';
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

    constructor(private auth: AuthService, public router: Router) {
    }

    ngOnInit(): void {

        console.log(this.auth.currentUser);
        console.log(this.router.url);

    }

}
