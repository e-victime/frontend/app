import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidator} from '../../helpers/custom-validator';
import {PosteService} from '../../services/poste/poste.service';
import {environment} from '../../../environments/environment';

declare let $: any;

@Component({
    selector: 'app-admin-poste-add',
    templateUrl: './admin-poste-add.component.html',
    styleUrls: ['./admin-poste-add.component.scss']
})
export class AdminPosteAddComponent implements OnInit {

    posteForm: FormGroup;
    success: object;
    error: object;

    constructor(private fb: FormBuilder, private CV: CustomValidator, private posteService: PosteService) {
    }

    ngOnInit(): void {

        // JQUER INIT

        this.jqueryInit();

        // FORM

        this.posteForm = this.fb.group(
            {
                posteName: ['',
                    [
                        Validators.required
                    ]
                ],
                responsable: ['',
                    []
                ],
                dateBegin: ['',
                    [
                        Validators.required
                    ]
                ],
                dateEnd: ['',
                    [
                        Validators.required
                    ]
                ],
                location: ['',
                    [
                        Validators.required
                    ]
                ],
                description: ['',
                    [
                        Validators.required
                    ]
                ]
            }
        );

    }

    hasProp(o, name) {
        return o.hasOwnProperty(name);
    }

    get form() {
        return this.posteForm.controls;
    }

    formEncoded() {

        const dateBegin = this.form.dateBegin.value.replace('T', ' ') + ':00';
        const dateEnd = this.form.dateEnd.value.replace('T', ' ') + ':00';
        return {
            nom: this.form.posteName.value,
            dateDebut: dateBegin,
            dateFin: dateEnd,
            location: this.form.location.value,
            descriptionPoste: this.form.description.value
        };

    }

    onSubmit() {

        this.success = null;
        this.error = null;

        document.getElementById('submit-form').classList.toggle('loading');

        this.posteService.add(this.formEncoded())
            .subscribe(r => {
                    this.success = r;

                    if (this.form.responsable.value) {

                        this.posteService.addResponsable(r.id, this.form.responsable.value)
                            .subscribe(() => {

                                document.getElementById('submit-form').classList.toggle('loading');
                                this.posteForm.reset();
                                $('.ui.search.selection.dropdown').dropdown('clear');

                            });

                    } else {

                        document.getElementById('submit-form').classList.toggle('loading');
                        this.posteForm.reset();
                        $('.ui.search.selection.dropdown').dropdown('clear');

                    }

                },
                error => {

                    this.error = error;
                    document.getElementById('submit-form').classList.toggle('loading');

                });

    }

    jqueryInit(): void {

        $('.ui.dropdown')
            .dropdown({
                onChange: (val) => {
                    this.form.responsable.setValue(val);
                },
                message: {
                    noResults: 'Pas de résultats trouvés...'
                },
                apiSettings: {
                    throttleFirstRequest: false,
                    throttle: 0,
                    minCharacters: 1,
                    url: environment.Api.Urls.personnel + 'search?search=(username:{query}* OR username:*{query}*)',
                    onResponse: (r) => {

                        const response = {
                            results: []
                        };

                        for (const personne of r.content) {

                            const result = {
                                name: '',
                                value: ''
                            };

                            result.name = `${personne.nom.toUpperCase()} ${personne.prenom} "${personne.username}"`;
                            result.value = personne.id;

                            response.results.push(result);

                        }

                        return response;

                    }
                }
            });

        $('.ui.large.red.button')
            .on('click', () => {
                $('.ui.search.selection.dropdown')
                    .dropdown('clear');
            });

    }

}
