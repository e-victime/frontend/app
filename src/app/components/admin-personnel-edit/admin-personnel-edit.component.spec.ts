import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPersonnelEditComponent } from './admin-personnel-edit.component';

describe('AdminPersonnelEditComponent', () => {
  let component: AdminPersonnelEditComponent;
  let fixture: ComponentFixture<AdminPersonnelEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPersonnelEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPersonnelEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
