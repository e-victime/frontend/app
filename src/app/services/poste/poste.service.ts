import {Injectable} from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PosteService {

    apiUrl: string = environment.Api.Urls.poste;

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private http: HttpClient) {
    }

    getAll(paginate: number = 0): Observable<any> {
        return this.http.get(environment.Api.Urls.poste + '?page=' + paginate);
    }

    get(id: string): Observable<any> {
        return this.http.get(environment.Api.Urls.poste + id);
    }

    getByName(name: string): Observable<any> {
        return this.http.get(environment.Api.Urls.poste + 'search?search=(nom:*' + name + '* OR nom:' + name + '*)');
    }

    getEquipeById(idEquipe: string) {
        return this.http.get(environment.Api.Urls.poste + 'search?search=id:' + idEquipe + '&size=100');
    }

    getEquipesOfPoste(id: string): Observable<any> {
        return this.http.get(environment.Api.Urls.poste + id + '/equipe');
    }

    getEquipeOfPoste(idPoste: string, idEquipe: string): Observable<any> {
        return this.http.get(environment.Api.Urls.poste + idPoste + '/equipe/' + idEquipe);
    }

    getEquipesOfPersonnel(idPersonnel: string): Observable<any> {
        return this.http.get(environment.Api.Urls.poste + 'search/membres?search=id.personnelId:' + idPersonnel + '&size=200');
    }

    getResponsable(posteId: string): Observable<any> {
        return this.http.get(environment.Api.Urls.poste + posteId + '/responsable');
    }

    add(val: object): Observable<any> {
        return this.http.post(environment.Api.Urls.poste, JSON.stringify(val), this.httpOptions);
    }

    addResponsable(posteId: string, personnelid: string): Observable<any> {
        return this.http.post(environment.Api.Urls.poste + posteId + '/responsable?idResponsable=' + personnelid, this.httpOptions);
    }

    addEquipe(posteId: string, val: object): Observable<any> {
        return this.http.post(environment.Api.Urls.poste + posteId + '/equipe', JSON.stringify(val), this.httpOptions);
    }

    addChefEquipeToEquipe(idPoste: string, idEquipe: string, idPersonnel: string): Observable<any> {
        return this.http.post(environment.Api.Urls.poste + idPoste + 'equipe/' + idEquipe + '/defChef?idChef=' + idPersonnel, this.httpOptions);
    }

    addMembreToEquipe(posteId: string, equipeId: string, val: object) {
        return this.http.post(environment.Api.Urls.poste + posteId + '/equipe/' + equipeId + '/personnel', JSON.stringify(val), this.httpOptions);
    }

    deleteMembreFromEquipe(posteId: string, equipeId: string, personnelId: string) {
        return this.http.delete(environment.Api.Urls.poste + posteId + '/equipe/' + equipeId + '/personnel/' + personnelId, this.httpOptions);
    }

    edit(id: string, val: object): Observable<any> {
        return this.http.put(environment.Api.Urls.poste + id, JSON.stringify(val), this.httpOptions);
    }

    editEquipe(idPoste: string, idEquipe: string, val: object): Observable<any> {

        return this.http.put(environment.Api.Urls.poste + idPoste + '/equipe/' + idEquipe, JSON.stringify(val), this.httpOptions);

    }

    delete(id: number): Observable<any> {
        return this.http.delete(environment.Api.Urls.poste + id);
    }


}
