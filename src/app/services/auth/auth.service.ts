import {Injectable} from '@angular/core';
import {Router} from "@angular/router";
import {AngularFireAuth} from "@angular/fire/auth";
import * as firebase from 'firebase';
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    public user: any;

    constructor(public afAuth: AngularFireAuth, public router: Router) {
        this.afAuth.authState.subscribe(user => {
            if (user) {
                this.user = user;
                console.log(this.user);
                localStorage.setItem('user', JSON.stringify(this.user));
            } else {
                localStorage.setItem('user', null);
            }
        });
    }

    login(email: string, password: string): Promise<firebase.auth.UserCredential> {
        return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    get currentUser() {
        return JSON.parse(localStorage.getItem("user"));
    }

    refreshAuthState() {
        return this.afAuth.authState;
    }

    manualDefineUser(val) {
        this.user = val;
        localStorage.setItem('user', JSON.stringify(this.user));
    }

    logout(): void {
        this.afAuth.auth.signOut();
        localStorage.removeItem('user');
        this.redirect();
    }

    redirect(): string {
        return environment.auth.redirectAfterLogin;
    }

    get isAdmin(): boolean {
        return JSON.parse(localStorage.getItem('user'))?.email && JSON.parse(localStorage.getItem('user')).email === "admin@admin.com";
    }

    loginState(): Observable<firebase.User | null> {
        return this.afAuth.authState;
    }

    get isLoggedIn(): boolean {
        return JSON.parse(localStorage.getItem("user"));
    }

}
