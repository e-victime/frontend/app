import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class PersonnelService {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    constructor(private http: HttpClient) {
    }

    get(id: string): Observable<any> {
        return this.http.get(environment.Api.Urls.personnel + id);
    }

    getAll(paginate: number = 0): Observable<any> {
        return this.http.get(environment.Api.Urls.personnel + '?page=' + paginate);
    }

    getByUsernameStrict(username: string): Observable<any> {
        return this.http.get(environment.Api.Urls.personnel + 'search?search=username:' + username);
    }

    getByEmailStrict(email: string): Observable<any> {
        return this.http.get(environment.Api.Urls.personnel + 'search?search=email:' + email);
    }

    getByUsername(username: string): Observable<any> {
        return this.http.get(environment.Api.Urls.personnel + 'search?search=username:*' + username + '* OR username:' + username + '*');
    }

    add(val: object): Observable<any> {
        return this.http.post(environment.Api.Urls.personnel, JSON.stringify(val), this.httpOptions);
    }

    edit(id: string, val: object): Observable<any> {
        return this.http.put(environment.Api.Urls.personnel + id, JSON.stringify(val), this.httpOptions);
    }

    delete(id: string): Observable<any> {
        return this.http.delete(environment.Api.Urls.personnel  + id, this.httpOptions);
    }

}
