import {Interface} from "readline";
import {CiviliteEnum} from "./civilite.enum";

export class Personnel {

    id: number;
    actif: boolean;
    civilite: CiviliteEnum;
    date_naissance: Date;
    email: string;
    firebase: string;
    nom: string;
    prenom: string;
    username: string;
    password: string;

    constructor(id, username, password, prenom, nom, actif, civilite, dateNaissance, email) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.prenom = prenom;
        this.nom = nom;
        this.actif = actif;
        this.date_naissance = dateNaissance;
        this.civilite = civilite;
        this.email = email;
    }

}
