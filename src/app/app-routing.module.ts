import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {AuthRoutes} from "./routes/auth";
import {UserRoutes} from "./routes/user";


@NgModule({
    imports: [
        RouterModule.forRoot(
            AuthRoutes,
            {
                enableTracing: true
            }
        ),
        RouterModule.forRoot(
            UserRoutes,
            {
                enableTracing: true
            }
        )
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
