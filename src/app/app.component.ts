import {Component} from '@angular/core';
import {NotificationService} from './services/notification/notification.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'e-victime';

    constructor(private notificationService: NotificationService) {
    }

    // tslint:disable-next-line:use-lifecycle-interface
    ngOnInit() {
        this.notificationService.createNotification('my notification', 'This is my notification and you can click me !');

    }

}
